import csv
import time
import re

from bs4 import BeautifulSoup
import requests
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

url = 'http://www.tripadvisor.in/Hotels-g186338-London_England-Hotels.html'

driver = webdriver.Firefox()
print "driver"
driver.get(url)


hotels = []
breaker = False

while True:
    html = driver.page_source
    soup = BeautifulSoup(html)
    listings = soup.select('div.listing')

    for l in listings:
        hotel = {}
        hotel['name'] =  l.select('a.property_title')[0].text
        hotel['rating'] = float(l.select('img.sprite-ratings')[0]['alt'].split('of')[0])
        hotels.append(hotel)

    next = driver.find_element_by_link_text('Next')
    if not next or breaker:
        break
    else:
        next.click()
        breaker = True
        time.sleep(0.5)

if len(hotels) > 0:
    with open('ratings.csv', 'w') as f:
        fieldnames = [ k for k in hotels[0].keys() ]
        writer = csv.DictWriter(f,fieldnames=fieldnames)
        writer.writeheader()
        for h in hotels:
            writer.writerow(h)

driver.quit()
