import numpy as np
import pylab
import mahotas as mh

from scipy import ndimage

EGGS1 = "imgs/47A04OreR_None_BF_MMStack.ome.tif"
EGGS2 = "imgs/44E10OreR_300uM+OA_BF_MMStack.ome.tif"
EGGS3 = "imgs/44E10OreR_200uM+OA_BF_MMStack.ome.tif"
EGGS1B = "imgs/47A04OreR_None_dsred_MMStack.ome.tif"
EGGS2B = "imgs/44E10OreR_300uM+OA_dsred_MMStack.ome.tif"
EGGS3B = "imgs/44E10OreR_200uM+OA_dsred_MMStack.ome.tif"

eggs = mh.imread(EGGS2)
eggsb = mh.imread(EGGS2B)
eggsf = ndimage.gaussian_filter(eggs, 1)
eggsbf = ndimage.gaussian_filter(eggsb, 4)

T = mh.thresholding.otsu(eggsf)
Tb = mh.thresholding.otsu(eggsbf)

rmax = mh.regmax(eggsbf)

labeled, num_objects = mh.label(eggsf > T)
labeledb, num_objectsb = mh.label(eggsbf > Tb)
print "num_objects: %s" % num_objects
print "num_objects b: %s" % num_objectsb
pylab.imshow(eggsbf)
pylab.show()

# pylab.imshow(labeledb)
# pylab.show()
