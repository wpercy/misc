import twython
import datetime
from twitter.constants import UCONN_APP_KEY, UCONN_APP_SECRET


def get_twitter_auth():
    twitter = Twython(UCONN_APP_KEY, UCONN_APP_SECRET, oauth_version=2)
    ACCESS_TOKEN = twitter.obtain_access_token()
    return Twython(APP_KEY, access_token=ACCESS_TOKEN)

def get_users_tweets(screen_name, max_tweets=200, **xtra_params):

    twitter = get_twitter_auth()
    params = {
        count: max_tweets,
        screen_name: screen_name
    }
    params.update(xtra_params)

    user_feed = twitter.get_user_timeline(**params)

    tweets = list()
    for tweet in user_feed:
        t = dict()
        t['content'] = tweet['text']
        t['time_created'] = format_datetime(tweet['created_at'])

        # entities include urls, hashtags, mentions, media and symbols
        entities = tweet['entities']
        t['urls'] = [ url['url'] for url in entities['urls'] ]
        t['hashtags'] = [ h['text'] for h in entities['hashtags'] ]


        tweet_list.append(t)

    return dict(user=screen_name, tweets=tweets)

def format_datetime(time_created):
    time_parts = time_created.split()
    timezone = time_parts[4]

    new_time = time_parts[1:4]
    new_time.append(time_parts[-1])

    the_time = datetime.strptime(' '.join(new_time), "%b %d %H:%M:%S %Y")
    return the_time.strftime("%Y-%m-%d %H:%M:%S")



